# Architect
Installation de Arch

## Pré-requis


Mettre le clavier en Azerty si besoin
```bash
loadkeys fr     #ou loqdkeys fr
```
Se connecter au WiFi
```bash
wifi-menu
```
Si une erreur de connexion s'affiche
```bash
ifconfig wlan0 down
```

## Usage
Installer git, clôner le git et donner les droits d'executions aux scripts
```bash
pacman -Sy git
git clone https://framagit.org/ourssansplus/architect.git
chmod +x architect/*
./architect/install.sh
```
et laissez vous guider ...

*ATTENTION* - Ce script est ecrit pour ma configuration personnelle sur un laptop avec un disque dur en NVMe.
La partie Wi-FI peut ne pas vous être utile si sous utilisez un câble réseau. Aussi, modifiez le fichier install.sh en remplacant nvme0n1 par sda (ou le nom d'un autre disque).
Certains logiciels peuvent être ajoutés ou retirés au pacstrap.
Toute contribution pouvant améliorer ce projet est la bienvenue.

## Contributions
- Script original par *oursSansPlus*
- Modification du fichier "partitions" pour plus de cohérence par *Lomig & Tux*